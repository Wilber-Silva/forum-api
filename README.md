# README #

 O projeto foi desenvolvido usando node v12 e serverless

* Não forma implementados testes unitarios para poupar tempo optei por implementar testes end2end usando nightwatch no projeto do front-end
* Estou usando uma dependencia minha que esta em processo de evolução e pode ser instalada com 'npm install --save will-core-lib'

## Requisitos ##

* node v12 ou docker
* (Opcional) configurar base local e atualizar a atribuição em config/.env

## Executar projeto ##

* Baixar dependencias 'npm install' ou 'npm update'
* Executar 'npm start' ou 'docker-compose -f docker-compose.yml up -d'
* Verificar no console se a x-api-key foi gerada
* (Opicional) É posivel executar as funções sem dar start usando np´m run invoke:local nomeDaFuncao

## Em caso de erros de conexão ##

* Verificar a conexão com MongoGB está correta
* Verificar se o DB esta em ativo
* (Caso persista) Subir um DB local

## Estrutura das pastas ##

* O projeto como um todo esta em src/
* config/ contem os arquivos de configuração do projeto
* functions/ contem o orquestrador que valida as chamadas http e executa o que a nomenclatura sugere
* Models/ contem os Schemas do DataBase
* Repositories/ conecta as funções aos models
* Validators/ contem os schemas para validar as requisições http

## Sobre o DB ##

* A base de dados escolhida foi MongoDB
* Optei por separar as perguntas e respostas obtendo o seguinte modelo

Perguntas (questions)

```json
  {
    "title": "String",
    "text": "String",
    "userName": "String",
    "visible": "Boolean",
    "createdAt": "Date",
    "updatedAt": "Date",
    "deletedAt": "Date"
  }
```

Respostas (answers)

```json
  {
    "questionId": "ObjectId",
    "text": "String",
    "userName": "String",
    "liked": "Boolean",
    "visible": "Boolean",
    "createdAt": "Date",
    "updatedAt": "Date",
    "deletedAt": "Date"
  }
```

* A view a baixo não fooi criada para poupar esforço ao rodar localmente em outra máquina:

```js
    db.createView(
      'view_questions_answer',
      'questions',
      [
            {
            $lookup:
              {
                from: "answers",
                as: "answers",
                let: { indicator_id: '$_id' },
                  pipeline: [
                    {
                        $match: {
                                visible: true,
                                deletedAt: null,
                                $expr: { $eq: [ '$questionId', '$$indicator_id' ] }
                        }
                    },
                    { $sort: { createdAt: -1 } }
                  ]
              }
            }
        ]
    )
```
