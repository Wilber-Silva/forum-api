FROM mhart/alpine-node:12

RUN npm install -g serverless

ADD . /var/www
WORKDIR /var/www

RUN npm install

ENV AWS_ACCESS_KEY_ID='wwww'
ENV AWS_SECRET_ACCESS_KEY='wwww'

EXPOSE 3003

CMD [ "serverless","deploy" ]
#CMD [ "npm","run", "start" ]
