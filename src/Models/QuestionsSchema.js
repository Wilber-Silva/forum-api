
const mongoose = require('mongoose')


module.exports.QuestionsSchema = new mongoose.Schema({
    title: { type: String },
    text: { type: String },
    userName: { type: String },
    visible: { type: Boolean, default: true },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    deletedAt: { type: Date, default: null }
})
