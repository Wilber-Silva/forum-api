
const mongoose = require('mongoose')

module.exports.AnswersSchema = new mongoose.Schema({
    questionId: { type: mongoose.Types.ObjectId },
    text: { type: String },
    userName: { type: String },
    visible: { type: Boolean, default: true },
    liked: { type: Boolean, default: false },
    createdAt: { type: Date, default: Date.now },
    updatedAt: { type: Date, default: Date.now },
    deletedAt: { type: Date, default: null }
})
