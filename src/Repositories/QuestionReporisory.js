const { BaseRepository, MongoConnection } = require("will-core-lib/connectors/mongodb")
const { QuestionsSchema } = require('../models/QuestionsSchema')


class QuestionsRepository extends BaseRepository {
    constructor() {
        super('questions', QuestionsSchema, new MongoConnection(process.env.MONGODB))
    }
}

module.exports = new QuestionsRepository()