const { BaseRepository, MongoConnection } = require("will-core-lib/connectors/mongodb")
const { AnswersSchema } = require('../models/AnswersSchema')

class AnswersRepository extends BaseRepository {
    constructor() {
        super('answers', AnswersSchema, new MongoConnection(process.env.MONGODB))
    }
}

module.exports = new AnswersRepository()