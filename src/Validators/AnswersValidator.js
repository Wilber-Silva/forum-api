const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi)

module.exports.create = Joi.object({
    questionId: Joi.objectId().required(),
    text: Joi.string().required(),
    userName: Joi.string().required()
})

module.exports.update = Joi.object({
    text: Joi.string().required(),
    liked: Joi.boolean().required()
})

module.exports.delete = Joi.object({
    id: Joi.objectId().required()
})