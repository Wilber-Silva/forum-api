const Joi = require('@hapi/joi')

module.exports.create = Joi.object({
    title: Joi.string().max(30),
    text: Joi.string().required(),
    userName: Joi.string().required()
})