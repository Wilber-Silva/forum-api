const {  ResponseError, ResponseNotContent, ResponseBadRequestException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const { delete: deleteSchema } = require('./../../Validators/AnswersValidator')

const AnswersRepository = require('./../../Repositories/AnswersRepository')

module.exports.handler = async ({ pathParameters }) => {
    try {
        const validation = await JoiValidation(deleteSchema, pathParameters)
        if (!validation.isValid) throw new ResponseBadRequestException(validation.message)

        await AnswersRepository.delete(pathParameters.id)

        return new ResponseNotContent()
    }
    catch (error) {
        console.log(error)
        return new ResponseError(error)
    }
}