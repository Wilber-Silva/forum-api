const {  ResponseError, ResponseOK, ResponseBadRequestException, ResponseUnprocessableEntityException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const { create: createSchema } = require('./../../Validators/AnswersValidator')

const QuestionRepository = require('./../../Repositories/QuestionReporisory')
const AnswersRepository = require('./../../Repositories/AnswersRepository')

module.exports.handler = async ({ body }) => {
    try {
        body = JSON.parse(body)

        const validation = await JoiValidation(createSchema, body)
        if (!validation.isValid) throw new ResponseBadRequestException(validation.message)


        const question = await QuestionRepository.findById(body.questionId)

        if (!question) throw new ResponseUnprocessableEntityException("Question not Exists")

        const answers = await AnswersRepository.create(body)

        return new ResponseOK(answers)
    }
    catch (error) {
        console.log(error)
        return new ResponseError(error)
    }
}