const {  ResponseError, ResponseOK } = require('will-core-lib/http')

const AnswersRepository = require('./../../Repositories/AnswersRepository')

module.exports.handler = async () => {
    try {

        const result = await AnswersRepository.update({}, { deletedAt: null })

        return new ResponseOK(result)
    }
    catch (error) {
        console.log(error)
        return new ResponseError(error)
    }
}