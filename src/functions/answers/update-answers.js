const {  ResponseError, ResponseNotContent, ResponseBadRequestException, ResponseUnprocessableEntityException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const { update: updateSchema } = require('./../../Validators/AnswersValidator')

const AnswersRepository = require('./../../Repositories/AnswersRepository')

module.exports.handler = async ({ body, pathParameters }) => {
    try {
        body = JSON.parse(body)

        const validation = await JoiValidation(updateSchema, body)
        if (!validation.isValid) throw new ResponseBadRequestException(validation.message)

        const answers = await AnswersRepository.findById(pathParameters.id)

        if (!answers) throw new ResponseUnprocessableEntityException('Answers not exists')

        answers.text = body.text
        answers.liked = body.liked

        await AnswersRepository.save(answers)

        return new ResponseNotContent()
    }
    catch (error) {
        console.log(error)
        return new ResponseError(error)
    }
}