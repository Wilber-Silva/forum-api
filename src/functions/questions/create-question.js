const {  ResponseError, ResponseOK, ResponseBadRequestException } = require('will-core-lib/http')
const { JoiValidation } = require('will-core-lib/validators')
const { create: createSchema } = require('./../../Validators/QuestionValidator')

const QuestionRepository = require('./../../Repositories/QuestionReporisory')

module.exports.handler = async ({ body }) => {
    try {
        body = JSON.parse(body)

        const validation = await JoiValidation(createSchema, body)
        if (!validation.isValid) throw new ResponseBadRequestException(validation.message)

        if (!body.title) body.title = body.text.substring(0, 30).replace(/(<([^>]+)>)/ig,"")

        const question = await QuestionRepository.create(body)
        question.answers = []
        return new ResponseOK(question)
    }
    catch (error) {
        console.log(error)
        return new ResponseError(error)
    }
}