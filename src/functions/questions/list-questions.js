const {  ResponseError, ResponseOK } = require('will-core-lib/http')

const QuestionAnswerRepository = require('../../Repositories/QuestionReporisory')

module.exports.handler = async () => {
    try {
        const questions = await QuestionAnswerRepository.aggregate([
            {
                $match: { 
                    visible: true,
                    deletedAt: null
                }
            },
            {
                $lookup:{
                    from: "answers",
                    as: "answers",
                    let: { indicator_id: '$_id' },
                        pipeline: [
                            { 
                                $match: {
                                        visible: true, 
                                        deletedAt: null,
                                        $expr: { $eq: [ '$questionId', '$$indicator_id' ] }
                                } 
                            },
                            { $sort: { createdAt: -1 } }
                        ]
                }
            }
        ])
        return new ResponseOK(questions)
    }
    catch (error) {
        console.log(error)
        return new ResponseError(error)
    }
}